exports.config = {
  paths: {
    watched: ['src'],
    public:  'dist',
  },
  files: {
    javascripts: {
      joinTo: 'scripts/mb-style.js'
    },
    stylesheets: {
      joinTo: 'styles/mb-style.css'
    }
  }
};
